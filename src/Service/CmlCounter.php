<?php

namespace Drupal\cmlapi\Service;

/**
 * Class Cml Exchange Counter.
 */
class CmlCounter {

  /**
   * Exchange Counter In Status New.
   */
  public function exchangeCounterInStatusNew() {
    // установлен ли syncloud.
    if (!\Drupal::service('module_handler')->moduleExists('syncloud')) {
      return;
    }
    // Uuid сайта, присваивается синклаудом при первой инициализации.
    $uuid = \Drupal::state()->get('syncloud.uuid');
    // домен сайта.
    $http_host = \Drupal::request()->getSchemeAndHttpHost();
    $service = \Drupal::service('cmlapi.cml');
    $cmllist = $service->new();
    $last_cml = $service->queryLastCml();

    // 'В очереди собралось ' . $counter . ' обменов со статусом new'
    $counter = count($cmllist);

    $msg = [
      'type' => 'cml-exchange',
      'last_created' => $last_cml[0]->created ?? '',
      'host' => \Drupal::request()->getHost(),
      'http_host' => $http_host,
      'counter' => $counter,
      'last_cml' => $last_cml,
    ];
    \Drupal::service('syncloud.mqtt')->run()->publish("\$cmlapi/counter/$uuid", json_encode($msg));
    // dsm('отправил');.
  }

}
