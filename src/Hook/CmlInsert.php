<?php

namespace Drupal\cmlapi\Hook;

/**
 * Implements Hook CmlInsert.
 */
class CmlInsert {

  /**
   * Hook.
   */
  public static function hook() {
    // при создании каждого файла cml запускаем сервис отправки данных о файле в дашборд на красной.
    \Drupal::service('cmlapi.counter')->exchangeCounterInStatusNew();
  }

}
